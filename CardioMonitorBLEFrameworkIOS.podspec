
Pod::Spec.new do |spec|

  spec.name         = "CardioMonitorBLEFrameworkIOS"
  spec.version      = "0.0.3"
  spec.summary      = "Framework by MircodCompany"

  spec.description  = "MircodIOSFramework is an easy to use Swift framework for working with Mircod company devices."
  spec.homepage     = "https://bitbucket.org/mircod_team/CardioMonitorBLEFrameworkIOS"
  spec.license      =  { :type => 'MIT', :text => <<-LICENSE
  Copyright (c) 2020 Mircod LLC <exit>

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to deal
  in the Software without restriction, including without limitation the rights
  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  THE SOFTWARE.

  LICENSE
  }

  spec.author       =  { "Мicod" => "" }
  spec.platform     = :ios, "11.0"
  spec.ios.vendored_frameworks = 'MircodIOSFrameworkSwift.framework'
  spec.source       =  { :git => 'https://bitbucket.org/mircod_team/MircodiOSFrameworkSource/MircodIOSFrameworkSwift.framework' } #, :tag => "0.2.0" }
  spec.ios.dependency 'iOSDFULibrary', '~> 4.1.3'
  # spec.exclude_files = "Classes/Exclude"
  # spec.source_files  = '*.zip'
  spec.swift_version = '5.0'

end
